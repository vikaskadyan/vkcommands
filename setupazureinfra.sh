#!/bin/bash
printf '%s\n' "**************  Installing Java 11 on machine *************" &&
sudo su
apt-get update &&
apt-get install -y openjdk-11-jre openjdk-11-jdk &&
printf '%s\n' "**************  Setting JAVA_HOME & JRE_HOME *************" &&
echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" >> /etc/environment &&
echo "JRE_HOME=/usr/lib/jvm/java-11-openjdk-amd64/jre" >> /etc/environment &&
source /etc/environment &&
echo $JAVA_HOME &&
printf '%s\n' "**************  Installing Maven on Machine *************" &&
apt update &&
apt install -y maven &&
mvn -version &&
printf '%s\n' "**************  Installing Jenkins on Machine *************" &&
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add - &&
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list' &&
apt update &&
apt install -y jenkins &&
systemctl start jenkins &&
systemctl enable jenkins &&
printf '%s\n' "**************  Installing Nginx on Machine *************" &&
apt-get update &&
apt-get install -y nginx-full &&
apt-get install -y certbot python-certbot-nginx &&
cd /etc/nginx/sites-available &&
touch jenkins.kadyan.de &&
cat <<EOT>> /etc/nginx/sites-available/jenkins.kadyan.de
server {
    server_name jenkins.kadyan.de;
    location / {
	proxy_set_header Host $host:$server_port;
	proxy_set_header X-Real-IP  $remote_addr;
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	proxy_set_header X-Forwarded-Proto $scheme;
    proxy_pass "http://127.0.0.1:8080";
    }
}
EOT
cd ../sites-enabled &&
ln -s ../sites-available/jenkins.kadyan.de &&
systemctl restart nginx &&
cd /etc/default &&
systemctl restart jenkins &&
printf '%s\n' "**************  Installing Certbot on Machine *************" &&
apt install certbot python-certbot-nginx &&
printf '%s\n' "**************  Installing Docker on Machine *************" &&
apt-get install -y apt-transport-https ca-certificates curl software-properties-common &&
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &&
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" &&
apt-get update &&
apt-get install -y docker-ce &&
systemctl enable docker && 
systemctl start docker && 
usermod -aG docker ubuntu &&
groupadd docker &&
usermod -aG docker jenkins &&
chmod 777 /var/run/docker.sock &&
printf '%s\n' "**************  Installing Azure CLI on Machine *************" &&
apt-get update &&
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash &&
az aks install-cli