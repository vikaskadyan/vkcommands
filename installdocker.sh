#!/bin/bash
printf '%s\n' "**************  Installing Docker on machine *************" &&
printf '%s\n' "**************  Installing Required package *************" &&

if [ -f "/var/cache/apt/archives/lock" ]; then
printf '%s\n' "inside lock if condition" && sudo rm /var/cache/apt/archives/lock
fi

if [ -f "/var/lib/dpkg/lock" ]; then
printf '%s\n' "inside dpkg/lock if condition" && sudo rm /var/lib/dpkg/lock
fi

if [ -f "/var/lib/dpkg/lock-frontend" ]; then
printf '%s\n' "inside lock-forward if condition" && sudo rm /var/lib/dpkg/lock-frontend
fi

sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common &&
printf '%s\n' "**************  Adding Docker key  *************" &&
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &&
printf '%s\n' "**************  Adding Repository *************" &&
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" &&
printf '%s\n' "**************  Update Machine *************" &&
sudo apt-get update &&
printf '%s\n' "**************  Installing Docker on machine *************" &&
sudo apt-get install -y docker-ce &&
printf '%s\n' "**************  Enable Docker on machine *************" &&
sudo systemctl enable docker &&
printf '%s\n' "**************  Start Docker on machine *************" &&
sudo systemctl start docker &&
printf '%s\n' "**************  Adding User As Docker on machine *************" &&
sudo usermod -aG docker cloud_user &&
printf '%s\n' "**************  Installing Docker on machine *************" &&
sudo chmod 777 /var/run/docker.sock &&
sudo reboot